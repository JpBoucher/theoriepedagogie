# Sentiment d'efficacité personnel (SEP) 

**Définition** : 
- L'évaluation que l'étudiant a de sa compétence. 
- La croyance dans sa capacité de réaliser une action précise. 
- Un sentiment de sa capacité d'agir. 
- En joual québécois : J'tu capab ?

**Pourquoi c'est important le SEP?** 

Si je me sens capable et que je me sens en contrôle de ma propre réussite, alors les études notent que :
1. Augmentation des efforts 
2. Augmentation de la persévérance
3. Augmentation de la résilience
4. Diminution du stress
5. Augmentation du niveau de réussite
6. Augmentation de l'ambition et des buts possibles à atteindre

Est-ce que je m'essaie ou que j'évite ? Le SEP.

Est-ce que je perçois un travail/examen comme un défi ou un obstacle ? Le SEP.

Pas un bon SEP ? Alors, je ne contrôle rien et tout repose sur des conditions externes.

**SEP et estime de soi**

Le SEP n'est pas relié à l'estime de soi. On peut ne pas être capable de faire une action précise sans que ça affecte notre estime. 

Le SEP c'est l'évaluation de notre aptitude dans un domaine donné.

L'estime de soi c'est l'évaluation de notre valeur personnelle.

**Sources du SEP**

1. Maîtrise personnelle

    La source principale. L'objectif qu'on veut que nos étudiants vivent.
    
    Source bidirectionnelle, les succès augmentent le SEP, mais les échecs diminuent le SEP.

    Cependant, pour que les succès aient un effet positif sur le SEP il faut que l'étudiant attribue la réussite à ses aptitudes et non pas à des éléments hors de leur contrôle.

2. Apprentissage social (vicariant)

    Observer les actions et les processus des autres permet d'augmenter son SEP, et surtout de vivre des expérience de maîtrise personnelle plus tard.

    Le fait d'être plus à l'aise dans une tâche que les autres est aussi source de SEP, par contre l'inverse est aussi vrai. Alors on tombe un peu dans un jeu à somme nulle.

    Plus le sujet est proche de nous (facteurs sociaux, compétences, ...), plus l'apprentissage social est susceptible d'être source de SEP.

3. Persuasion par autrui

    La persuasion permet surtout de maintenir un SEP, ainsi c'est moins une source qu'un outil qui permet d'éviter de perdre son SEP.

    La persuasion est particulièrement efficace quand elle est fait par une personne vue significative.

    Il faut être prudent de ne pas mener à l'échec après avoir susciter des croyances irréalistes. Il y aura alors perte de SEP et discréditation de la personne qui a usé de persuasion.

4. État physiologique et émotionnel

    Le plaisir, ou son contraire, qu'on ressent lors de la réalisation d'une activité à un impact sur le niveau de réussite et peut changer un échec et vice-versa.

    Une bonne gestion du stress évite le sentiment de perte de contrôle qui impacte négativement le SEP.

**Recommandation concrètes pour un professeur soucieux du SEP**

- Éviter de juger la difficulté des cours/travaux/examens. Par exemple :
    - "Attention, le cours d'aujourd'hui sera difficile" (encore pire, le cours de la semaine prochaine)
    - "On va faire un exercice facile" (si on veut faire ce genre de commentaires, toujours le justifier par quelque chose que l'étudiant contrôle)
- Faire du formatif avec de la rétroaction (formelle ou informelle)
    - Permet de multiplier les expériences de maîtrise
    - Permet d'user de persuasion
    - Permet de réduire le stress
- Faire des travaux d'équipe (bien encadré)
    - Permet de stimuler les expériences vicariantes
- Viser un climat d'apprentissage positif exempt de sources de stress inutiles
- Utiliser des [questionnaires d'attribution causale](/QAT.md)
    - Permet aux étudiants de voir le contrôle qu'ils ont sur leur réussite ou échec et d'éviter qu'ils se réfugient dans des facteurs hors de leur contrôle (le prof est chien, l'examen est trop difficile, le cours est à 8h, ...)
- Viser la [zone proximale de développement](/ZPD.md)
    - Permet de vivre des expériences de maîtrise significatives
    - Permet de vivre des expériences vicariantes
- « Un petit succès qui persuade l’individu qu’il possède tout ce qu’il faut pour réussir lui permet de s’élever ensuite bien au-dessus de cette performance » Bandura (2003)


**Référence pour le SEP**

Excellente synthèse : Le SEP et la réussite au collégial de Gaudreau (2013) https://eduq.info/xmlui/bitstream/handle/11515/21908/Gaudreau-26-3-2013.pdf?sequence=1&isAllowed=y

La bible : L'auto-efficacité de Bandura (2003) https://fxg.koha.collecto.ca/cgi-bin/koha/opac-detail.pl?biblionumber=70145

[Retour à l'accueil](/README.md)