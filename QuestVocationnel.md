# Questionaire vocationnel

Attention, ce concept n'est pas ancré dans un référentiel théorique (pas de recherche ou même un concept précis pour le questionnaire vocationnel). Par contre, l'identité professionnel et la clarté vocationnelle sont souvent citées dans les travaux sur la motivation des étudiants. 

**Définition**

Questionnaire qui a pour but d'amener les étudiants à réfléchir à leur identité professionnelle et à revisiter leur choix vocationnel.

**Pourquoi l'utiliser?**

Parce que le choix du programme au Cégep est :
1. Fait à un très jeune âge
2. Fait avec très peu de connaissances (et probablement beaucoup de préjugés) des programmes
3. Souvent basé par des influences externes (amis, parents, la fille/le gars du secondaire sur qui on a kick, ...)
4. N'est habituellement pas remis en question si l'étudiant réussi bien

Le questionnaire permet de :
1. Se placer en contexte de [pensée réflexive](/PenseeReflexive.md)
2. Développer son identité professionnelle et améliorer sa clarté vocationnelle
3. Influencer positivement la motivation scolaire grâce au #2

**Quand l'utiliser?**

- Pas trop tôt dans le cheminement, car l'étudiant n'aura peut-être pas assez vécu dans le programme pour que le questionnaire soit pertinent
- Pas trop tard dans le cheminement, car si l'étudiant se rend compte d'un conflit vocationnel entre son programme et ses visées, il est tard pour corriger le conflit. En raison du concept de [coût irrécupérable](https://fr.wikipedia.org/wiki/Co%C3%BBt_irr%C3%A9cup%C3%A9rable) il est probable qu'il opte de continuer et de finir "tant qu'à ça" 


**Exemple**

https://docs.google.com/forms/d/e/1FAIpQLSdie51_1i3PeycO27d9oo8OPFCBWbt41ZdGmIJ9qP2qJqH1cg/formResponse

[Retour à l'accueil](/README.md)