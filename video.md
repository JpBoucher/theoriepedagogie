# Quelques vidéos inspirantes

[Ceci va révolutionner l'éducation ... ou pas!](https://www.youtube.com/watch?v=GEmuEWjHr5c)

[Changer le paradigme de l'éducation](https://www.youtube.com/watch?v=zDZFcDGpL4U)

[Mr. Pike](https://www.youtube.com/watch?v=2NlMSel41Ww)

[Accepter la différence](https://www.youtube.com/watch?v=vnvS4JxdCQ4)

[Retour à l'accueil](/README.md)