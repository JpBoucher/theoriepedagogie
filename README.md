# Théorie de la pédagogie

Le contenu de cette page est une sélection bien subjective de théories pédagogiques et de contenus qui m'inspirent .

- L'étudiant
    - [SEP](/SEP.md)
    - [Zone Proximale](/ZPD.md)

- La Pensée réflexive
    - [Pensée réflexive](/PenseeReflexive.md)
    - [Métacognition](/Metacognition.md)
    - [Questionnaire d'attribution causale](/QAT.md)
    - [Questionnaire vocationnel](/QuestVocationnel.md)

- L'évaluation
    - L'évaluation : un acte d'enseignement
    - Grille critériée
    - Rétroaction audio
    - Rétroaction audio-vidéo

[Vidéos](/video.md)