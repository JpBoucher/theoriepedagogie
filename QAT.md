# Questionnaire d'attribution causale

**Définition**

Questionnaire qui a pour but d'amener les étudiants à réfléchir aux causes de leurs succès et de leurs échecs.

**Pourquoi l'utiliser?**

- Place les étudiants en situation de [pensée réflexive](/PenseeReflexive.md)
- Fait prendre conscience du contrôle qu'ils ont sur leurs résultats (voir le [SEP](/SEP.md))

**Quand l'utiliser?**

- Avec n'importe quel travaux qui demande une préparation hors classe

**Comment ça fonctionne?**

- 3 petits questionnaire à des moments précis
    - Avant l'évaluation
        - Est-ce que je suis prêt ?
        - Pourquoi je suis prêt ou non ?
        - Comment je me suis préparer ?
    - Au moment de la fin de l'évaluation
        - Est-ce que mon évaluation a bien été ?
        - Qu'est-ce qui a bien été ? 
        - Qu'est-ce qui a moins bien été ?
        - Je m'attends à une note de :
    - À la remise des évaluations corrigées
        - Est-ce que ma note attendue était proche de la note réelle ?
        - Est-ce que les rétroactions était celles que je m'attendais ?
        - Pourquoi est-ce différent ou semblable à ce que j'avais anticipé ?
        - Est-ce que je devrais changer ma préparation ? Si oui, que devrais-je faire ?

**Recommandation**

- Être encadrant, surtout au début, les étudiants n'ont probablement pas l'habitude de la pensée réflexive
- Réserver du temps en classe pour le faire, c'est une activité pédagogique!
- Ne pas faire de trop gros questionnaire

**Référence pour le questionnaire d'attribution causale**

Article synthèse de Roberge : https://eduq.info/xmlui/bitstream/handle/11515/34658/roberge-29-3-2016.pdf

Beaucoup d'exemples de questionnaire dans la Recherche PARÉA de Roberge à compter de la page 443 : https://eduq.info/xmlui/bitstream/handle/11515/38572/Roberge-questionnaire-attribution-causale-claurendeau-PAREA-2022.pdf?sequence=5&isAllowed=y

[Retour à l'accueil](/README.md)