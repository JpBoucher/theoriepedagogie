# Zone Proximale de développement

![Zone proximale de développement](zpd.png "ZPD")

**Description**

La zone de ce qu'une personne peut apprendre avec l'aide d'une autre personne plus experte.

La zone proximale commence à la limite de ce que la personne peut faire par elle-même et se termine à la limite de ce qu'une personne peut faire avec de l'aide.

**Pourquoi la viser?**

[Retour à l'accueil](/README.md)