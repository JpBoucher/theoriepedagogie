# Métacognition

**Définition**

La connaissance qu'un individu a de ses propres processus mentaux et sa capacité de les réguler

Ultimement, c'est une des conséquences de la [pensée réflexive](/PenseeReflexive.md)

**Les trois étapes de la métacognition**
1. L'anticipation
2. Le contrôle
3. L'ajustement

[Retour à l'accueil](/README.md)