## Pensée réflexive

**Définition** 

La capacité d'un individu d'être l'objet de sa propre réflexion afin de porter un jugement sur ses comportements

**Pourquoi la stimuler?**

- Parce que ce n'est pas instinctif, ni facile de le faire
- Casser les comportements acquis (Parce que j'ai toujours été comme ça, Parce que la vie est comme ça) et développer l'apprentissage autonome (métacompétence)
- Prise de conscience du contrôle qu'ils ont sur leurs vies (voir le [SEP](/SEP.md))

**Comment le faire?**

- [Questionnaire d'attribution causale](/QAT.md)
- [Questionnaire vocationnel](/QuestVocationnel.md) (pour les programmes techniques)
- Oralement avec les étudiants
    - "Comment as-tu fait ce travail ?" suivi de "Est-ce que ça put expliquer le succès/l'échec ?
    - "Comment tu t'es préparer pour cet examen ?"
    - "Est-ce que tu te rappelles de ce que tu étais en mesure de faire à la semaine 1 ? Et maintenant ?" 

**Référence pour la pensée réflexive**

Article référence (p.311-334) : https://fxg.koha.collecto.ca/cgi-bin/koha/opac-detail.pl?biblionumber=170726&query_desc=%C3%89valuer%20les%20comp%C3%A9tences%20au%20coll%C3%A9gial%20et%20%C3%A0%20l%27universit%C3%A9%3A

Article synthèse (l'autrice utilise le terme Autoévalution au lieu de pensée réflexive) : https://pedagogie.uquebec.ca/le-tableau/autoevaluation-et-autoregulation-deux-habiletes-favorables-aux-apprentissages-en

[Retour à l'accueil](/README.md)